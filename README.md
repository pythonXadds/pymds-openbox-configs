# Idio'wiki

**add winfucs tiler to openbox** (adding dynamic tyling to openbox)

Winfuncs requires that xdotool, x11-utils, wmctrl and xautoclock, all small programs, be installed. It offers the user 5 tiling modes: tile, cascade, tiletwo, select and showdesktop. Put winfuncs in /usr/local/ bin/ (on the path) and make it executable. The commands to launch the 5 modes are
     winfuncs "tile"
     winfuncs "cascade"
     winfuncs "tiletwo"
     winfuncs "select"
     winfuncs "showdesktop".

xautoclock can't be found, and you will want to install "bc". everything else already installed.

`sudo apt install bc`
Download the script using wget:
Place in ~/bin and make executable.

`wget http://lxlinux.com/winfuncs`

Then set keybinds however you wish.

---

